const sql = require("./db.js");

// constructor
const Recipe = function(recipe) {
    this.name = recipe.name;
    this.category = recipe.category;
    this.picture = recipe.picture;
    this.score = recipe.score;
    this.ingredients_id = recipe.ingredients;
};

Recipe.create = (newRecipe, result) => {
    sql.query("INSERT INTO recipe SET ?", newRecipe, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        console.log("created recipe: ", { id: res.insertId, ...newRecipe });
        result(null, { id: res.insertId, ...newRecipe });
    });
};

Recipe.findById = (recipeId, result) => {
    sql.query(`SELECT * FROM recipe WHERE id = ${recipeId}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.length) {
            console.log("found recipe: ", res[0]);
            result(null, res[0]);
            return;
        }

        // not found recipe with the id
        result({ kind: "not_found" }, null);
    });
};

Recipe.findByingredients = result => {
    sql.query("SELECT * FROM recipe, ingredient WHERE recipe.ingredients_Id = ingredient.id; ", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        if (res.length) {
            res.forEach(row => {
                console.log("result N°" + row.index);
                result(null, row);
            });
            return;
        }
    })
}
Recipe.getAll = result => {
    sql.query("SELECT * FROM recipe", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log("recipes: ", res);
        result(null, res);
    });
};

Recipe.updateById = (id, recipe, result) => {
    sql.query(
        "UPDATE recipe SET name = ? WHERE id = ?", [recipe.name, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }

            if (res.affectedRows == 0) {
                // not found recipe with the id
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("updated recipe: ", { id: id, ...recipe });
            result(null, { id: id, ...recipe });
        }
    );
};

Recipe.remove = (id, result) => {
    sql.query("DELETE FROM recipe WHERE id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found recipe with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted recipe with id: ", id);
        result(null, res);
    });
};

Recipe.removeAll = result => {
    sql.query("DELETE FROM recipe", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log(`deleted ${res.affectedRows} recipes`);
        result(null, res);
    });
};


module.exports = Recipe;