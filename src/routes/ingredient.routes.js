module.exports = app => {
    const ingredient = require("../controllers/ingredient.controller.js");

    // Create a new ingredient
    app.post("/ingredient", ingredient.create);

    // Retrieve all ingredient
    app.get("/ingredient", ingredient.findAll);

    // Retrieve a single ingredient with ingredientId
    app.get("/ingredient/:ingredientId", ingredient.findOne);

    // Update a ingredient with ingredientId
    app.put("/ingredient/:ingredientId", ingredient.update);

    // Delete a ingredient with ingredientId
    app.delete("/ingredient/:ingredientId", ingredient.delete);

    // Create a new ingredient
    app.delete("/ingredient", ingredient.deleteAll);
};