module.exports = app => {
    const recipe = require("../controllers/recipe.controller.js");

    // Create a new recipe
    app.post("/recipe", recipe.create);

    // Retrieve all recipe
    app.get("/recipe", recipe.findAll);

    // Retrieve a single recipe with recipeId
    app.get("/recipe/:recipeId", recipe.findOne);

    // Update a recipe with recipeId
    app.put("/recipe/:recipeId", recipe.update);

    // Delete a recipe with recipeId
    app.delete("/recipe/:recipeId", recipe.delete);

    // Create a new recipe
    app.delete("/recipe", recipe.deleteAll);
};