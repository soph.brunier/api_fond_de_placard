const Ingredient = require("../models/ingredient.model.js");

// Create and Save a new ingredient
exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }
    const ingredient = new Ingredient({
        name: req.body.name
    });
    Ingredient.create(ingredient, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Some error occurred while creating the ingredient."
            });
        else res.send(data);
    })
};

// Retrieve all ingredients from the database.
exports.findAll = (req, res) => {
    Ingredient.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving ingredients."
            });
        else res.send(data);
    });
};

// Find a single ingredient with a ingredientId
exports.findOne = (req, res) => {
    Ingredient.findById(req.params.ingredientId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found ingredient with id ${req.params.ingredientId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving ingredient with id " + req.params.ingredientId
                });
            }
        } else res.send(data);
    });
};

// Update a ingredient identified by the ingredientId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    Ingredient.updateById(
        req.params.ingredientId,
        new Ingredient(req.body),
        (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.status(404).send({
                        message: `Not found ingredient with id ${req.params.ingredientId}.`
                    });
                } else {
                    res.status(500).send({
                        message: "Error updating ingredient with id " + req.params.ingredientId
                    });
                }
            } else res.send(data);
        }
    );
};

// Delete a ingredient with the specified ingredientId in the request
exports.delete = (req, res) => {
    Ingredient.remove(req.params.ingredientId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found ingredient with id ${req.params.ingredientId}.`
                });
            } else {
                res.status(500).send({
                    message: "Could not delete ingredient with id " + req.params.ingredientId
                });
            }
        } else res.send({ message: `ingredient was deleted successfully!` });
    });
};

// Delete all ingredients from the database.
exports.deleteAll = (req, res) => {
    Ingredient.removeAll((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Some error occurred while removing all ingredient."
            });
        else res.send({ message: `All ingredients were deleted successfully!` });
    });
};