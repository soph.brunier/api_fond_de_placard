const Recipe = require("../models/recipe.model.js");

// Create and Save a new recipe
exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }
    const recipe = new Recipe({
        name: req.body.name,
        category: req.body.category,
        picture: req.body.picture,
        score: req.body.score,
        ingredients: req.body.ingredients
    });
    Recipe.create(recipe, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Some error occurred while creating the recipe."
            });
        else res.send(data);
    })
};

// Retrieve all recipes from the database.
exports.findAll = (req, res) => {
    Recipe.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving recipes."
            });
        else res.send(data);
    });
};

// Find a single recipe with a recipeId
exports.findOne = (req, res) => {
    Recipe.findById(req.params.recipeId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found recipe with id ${req.params.recipeId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving recipe with id " + req.params.recipeId
                });
            }
        } else res.send(data);
    });
};

// Update a recipe identified by the recipeId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    Recipe.updateById(
        req.params.recipeId,
        new Recipe(req.body),
        (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.status(404).send({
                        message: `Not found recipe with id ${req.params.recipeId}.`
                    });
                } else {
                    res.status(500).send({
                        message: "Error updating recipe with id " + req.params.recipeId
                    });
                }
            } else res.send(data);
        }
    );
};

// Delete a recipe with the specified recipeId in the request
exports.delete = (req, res) => {
    Recipe.remove(req.params.recipeId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found recipe with id ${req.params.recipeId}.`
                });
            } else {
                res.status(500).send({
                    message: "Could not delete recipe with id " + req.params.recipeId
                });
            }
        } else res.send({ message: `recipe was deleted successfully!` });
    });
};

// Delete all recipes from the database.
exports.deleteAll = (req, res) => {
    Recipe.removeAll((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Some error occurred while removing all recipe."
            });
        else res.send({ message: `All recipes were deleted successfully!` });
    });
};