DROP DATABASE IF EXISTS api_fond_placard;
CREATE DATABASE api_fond_placard;
USE api_fond_placard;

CREATE TABLE recipe (
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name VARCHAR(128) NOT NULL,
    category VARCHAR(128),
    picture TEXT,
    score INT
);

CREATE TABLE ingredient(
    id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(128) NOT NULL
);

CREATE TABLE recipe_ingredient(
    recipe_id INT,
    ingredients_id INT
);

ALTER TABLE recipe_ingredient ADD FOREIGN KEY (recipe_id) REFERENCES recipe(id);
ALTER TABLE recipe_ingredient ADD FOREIGN KEY (ingredients_id) REFERENCES ingredient(id);

-- FIXTURES

INSERT INTO ingredient (name) VALUES ('tomate');
INSERT INTO ingredient (name) VALUES ('oignon');
INSERT INTO ingredient (name) VALUES ('courgette');
INSERT INTO ingredient (name) VALUES ('aubergine');
INSERT INTO ingredient (name) VALUES ('poivron');
INSERT INTO ingredient (name) VALUES ('thym');
INSERT INTO ingredient (name) VALUES ('huile');
INSERT INTO ingredient (name) VALUES ('beurre');
INSERT INTO ingredient (name) VALUES ('celeri');
INSERT INTO ingredient (name) VALUES ('champignon');


INSERT INTO recipe (name, category, picture, score) VALUES ('ratatouille', 'plats', '', 4);

INSERT INTO recipe_ingredient (recipe_id, ingredients_id) VALUES (1,1);
INSERT INTO recipe_ingredient (recipe_id, ingredients_id) VALUES (1,2);
INSERT INTO recipe_ingredient (recipe_id, ingredients_id) VALUES (1,3);
INSERT INTO recipe_ingredient (recipe_id, ingredients_id) VALUES (1,4);
INSERT INTO recipe_ingredient (recipe_id, ingredients_id) VALUES (1,5);
INSERT INTO recipe_ingredient (recipe_id, ingredients_id) VALUES (1,6);